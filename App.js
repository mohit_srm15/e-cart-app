/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {View} from 'react-native';
import 'react-native-gesture-handler';

import {AppContainer} from './src/Router';
import {LightText} from './src/Components/Common/StyledTexts';

function App() {
  console.log(' reached here ');
  return (
    <View style={{flex: 1}}>
      <AppContainer/>
    </View>
  );
}

export default App;
