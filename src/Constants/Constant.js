export const CONSTANTS = {
  PRODUCT_CATEGORIES: {
    fruitsAndVegetables: {
      label: 'Fruits & Vegetables',
      image: require('../assets/fruits.jpg'),
    },
    groceryProducts: {
      label: 'Grocery Products',
      image: require('../assets/grocery.jpg'),
    },
    dairyProducts: {
      label: 'Dairy Products',
      image: require('../assets/dairy_products.jpg'),
    },
    snacks: {
      label: 'Snacks & Chocolates',
      image: require('../assets/snacks.jpg'),
    },
  },
  CATEGORY_VARIETY: [
    {
      label: 'Food Grains',
      value: 'foodGrains',
    },
    {
      label: 'Pulses',
      value: 'pulses',
    },
    {
      label: 'Flours',
      value: 'flours',
    },
    {
      label: 'Edible Oils',
      value: 'oil',
    },
    {
      label: 'Spices',
      values: 'spices',
    },
  ],
  ITEMS_LIST: {
    fortuneOil_1: {
      label: 'Fortune Oil',
      description: 'Fortune everyday Basmati rice Full Day',
      quantity: '5kg',
      veg: true,
      image: require('../assets/fortune_oil.jpg'),
      rate: '149'
    },
    fortuneOil_2: {
      label: 'Fortune Oil',
      description: 'Fortune everyday Basmati rice Full Day',
      quantity: '10kg',
      veg: true,
      image: require('../assets/fortune_oil.jpg'),
      rate: '709'
    },
    basmatiRice_1: {
      label: 'Basmati Rice',
      description: 'Kohinoor authentic Royale Basmati rice',
      quantity: '0.5kg',
      veg: true,
      image: require('../assets/basmati_rice.jpg'),
      rate: '190'
    },
    basmatiRice_2: {
      label: 'Basmati Rice',
      description: 'Kohinoor authentic Royale Basmati rice',
      quantity: '5kg',
      veg: true,
      image: require('../assets/basmati_rice.jpg'),
      rate: '1490'
    },
  },
};
