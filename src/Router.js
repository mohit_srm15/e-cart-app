import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Splash from './Screens/Splash';
import Home from './Screens/Home';
import VendorItems from './Screens/VendorItems';

const appNavigations = createStackNavigator({
  Splash : {screen : Splash},
  Home: {screen: Home},
  VendorItems: {screen:  VendorItems}
} , {
  headerMode: 'none'
});

export const AppContainer = createAppContainer(appNavigations);
