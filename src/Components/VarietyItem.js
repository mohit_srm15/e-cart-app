import React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import {LightText} from './Common/StyledTexts';
import Color from '../Styles/Color';

function VarietyItem(props) {
  let {productItem} = props;

  return (
    <View style={{flex: 1}}>
      <View style={{flexDirection: 'row', flex: 1}}>
        <View
          style={{
            flex: 0.3,
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
          }}>
          <Image source={productItem.image} style={{width: 100, height: 100}} />
        </View>
        <View
          style={{
            flex: 0.12,
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}>
          <View
            style={{
              borderWidth: 3,
              borderColor: 'green',
              paddingHorizontal: 5,
              height: 20,
              justifyContent: 'center',
              alignItems: 'center',
              width: 20,
              marginHorizontal: 10,
            }}>
            <View
              style={{
                height: 10,
                width: 10,
                borderRadius: 10,
                backgroundColor: 'green',
              }}
            />
          </View>
        </View>
        <View style={{flex: 0.58, justifyContent: 'flex-start'}}>
          <View>
            <LightText style={{fontSize: 20, color: Color.gray04}}>
              {`${productItem.description} - ${productItem.quantity}`}
            </LightText>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon name={'rupee'} color={Color.gray02} size={20} />
              <LightText
                style={{fontSize: 25, color: Color.gray02, marginLeft: 5}}>
                {productItem.rate}
              </LightText>
            </View>
            <TouchableOpacity
              style={{
                borderWidth: 1,
                borderColor: Color.yellow,
                paddingHorizontal: 15,
                paddingVertical: 5,
                borderRadius: 10,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Icon name={'plus'} color={Color.yellow} size={12} />
                <LightText
                  style={{
                    color: Color.yellow,
                    fontSize: 20,
                    marginHorizontal: 5,
                  }}>
                  Add
                </LightText>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
}
export default VarietyItem;
