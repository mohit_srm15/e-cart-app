import React from 'react';
import {View, TouchableOpacity, ScrollView, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import {BoldText, LightText} from '../Components/Common/StyledTexts';
import Color from '../Styles/Color';

function Home(props) {
  let data = require('../Constants/task');
  return (
    <View style={{flex: 1}}>
      <View
        style={{
          flex: 0.1,
          backgroundColor: Color.gray05,
          paddingHorizontal: 10,
        }}>
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
            alignItems: 'center',
            paddingHorizontal: 5,
          }}>
          <View style={{flex: 0.88}}>
            <BoldText
              style={{
                color: Color.headerColor,
                fontSize: 35,
                letterSpacing: 10,
              }}>
              toneTag
            </BoldText>
          </View>
        </View>
      </View>

      <ScrollView style={{flex: 1}}>
        <View style={{padding: 20, marginVertical: 10}}>
          {data.data && data.data.length > 0
            ? data.data.map((storeInfo, i) => {
              if(i===0){
                console.log(" store unfo " , storeInfo);
              }
                return (
                  <TouchableOpacity
                    onPress={() =>
                      props.navigation.navigate('VendorItems', {
                        storeDetail: storeInfo,
                      })
                    }
                    key={`${i}`}
                    style={{
                      flexDirection: 'row',
                      flex: 1,
                      borderWidth: 1,
                      marginVertical: 20,
                      padding: 20,
                      borderRadius: 20,
                      backgroundColor: Color.gray06,
                      borderColor: Color.gray05,
                    }}>
                    <View style={{flex: 0.3}}>
                      <Image
                        source={
                          storeInfo.store_image
                            ? {uri: storeInfo.store_image}
                            : require('../assets/store_image.jpg')
                        }
                        style={{width: 90, height: 90, borderRadius: 45}}
                        resizeMode={'cover'}
                      />
                    </View>
                    <View
                      style={{
                        flex: 0.7,
                        justifyContent: 'center',
                        paddingLeft: 15,
                      }}>
                      <BoldText style={{color: Color.gray04}}>
                        {storeInfo.name ? storeInfo.name : 'N/A'}
                      </BoldText>
                      <View style={{flexDirection: 'row', marginVertical: 5}}>
                        <Image
                          source={require('../assets/location.png')}
                          style={{width: 25, height: 25}}
                        />
                        <View style={{justifyContent: 'center'}}>
                          <LightText
                            style={{marginLeft: 10, color: Color.gray02}}>
                            {storeInfo.address_line_1
                              ? storeInfo.address_line_1
                              : 'N/A'}
                          </LightText>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              })
            : null}
        </View>
      </ScrollView>
    </View>
  );
}

export default Home;
