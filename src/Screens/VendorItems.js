import React, {useEffect, useState} from 'react';
import {
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

import {BoldText, LightText} from '../Components/Common/StyledTexts';
import Color from '../Styles/Color';
import {CONSTANTS} from '../Constants/Constant';
import VarietyItem from '../Components/VarietyItem';

function VendorItems(props) {
  let {storeDetail} = props.navigation.state.params;
  let {PRODUCT_CATEGORIES, CATEGORY_VARIETY, ITEMS_LIST} = CONSTANTS;

  const [storeInfo, setStoreInfo] = useState({});
  const [activeCategory, setActiveCategory] = useState('fruitsAndVegetables');
  const [activeVariety, setActiveVariety] = useState({
    label: 'Food Grains',
    value: 'foodGrains',
  });

  useEffect(() => {
    if (storeDetail && Object.keys(storeDetail).length > 0) {
      setStoreInfo(storeDetail);
    }
  }, [props.navigation]);

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View style={{flex: 0.2}}>
        <View style={{flexDirection: 'row', flex: 1}}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'flex-start',
              flex: 0.88,
              paddingLeft: 10,
              paddingTop: 20,
            }}>
            <TouchableOpacity
              style={{paddingHorizontal: 5}}
              onPress={() => props.navigation.goBack(null)}>
              <Icon name={'arrowleft'} size={30} color={Color.gray04} />
            </TouchableOpacity>
            <View style={{marginLeft: 20}}>
              <BoldText
                style={{
                  fontWeight: 'bold',
                  color: Color.gray04,
                  fontSize: 22,
                  letterSpacing: 1,
                }}>
                {storeInfo.name ? storeInfo.name : 'N/A'}
              </BoldText>
              <LightText style={{fontSize: 17, marginVertical: 5}}>
                {storeInfo.address_line_1 ? storeInfo.address_line_1 : 'N/A'}
              </LightText>
            </View>
          </View>
          <View
            style={{
              flex: 0.12,
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
              paddingTop: 20,
            }}>
            <TouchableOpacity style={{ paddingHorizontal: 5}}>
              <Icon name={'search1'} size={30} color={Color.gray04} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <ScrollView style={{flex: 1}}>
        <View style={{paddingHorizontal: 20}}>
          <FlatList
            data={Object.keys(PRODUCT_CATEGORIES)}
            horizontal
            style={{paddingVertical: 10}}
            keyExtractor={(item, index) => index.toString()}
            ItemSeparatorComponent={() => {
              return <View style={{flex: 1, width: 10}} />;
            }}
            renderItem={({item}) => {
              return (
                <TouchableOpacity
                  onPress={() => setActiveCategory(item)}
                  style={{
                    width: 170,
                    height: 155,
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={PRODUCT_CATEGORIES[item].image}
                    style={{
                      width: 150,
                      height: 100,
                      borderRadius: 20,
                      borderWidth: 1,
                      borderColor: Color.gray06,
                    }}
                    resizeMode={'cover'}
                  />
                  <BoldText
                    style={{
                      marginVertical: 10,
                      fontSize: 20,
                      color: activeCategory === item ? 'gray' : Color.gray03,
                    }}>
                    {PRODUCT_CATEGORIES[item].label}
                  </BoldText>
                </TouchableOpacity>
              );
            }}
          />
        </View>
        <View style={{backgroundColor: Color.gray05, height: 10}} />
        <View style={{paddingVertical: 10}}>
          <FlatList
            data={CATEGORY_VARIETY}
            horizontal
            style={{paddingVertical: 10}}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => {
              return (
                <TouchableOpacity
                  onPress={() => setActiveVariety(item)}
                  style={{
                    paddingHorizontal: 20,
                    borderBottomWidth:
                      activeVariety.value === item.value ? 5 : 0,
                    borderColor: Color.yellow,
                    paddingVertical: 10,
                  }}>
                  <BoldText
                    style={{
                      color:
                        activeVariety.value === item.value
                          ? 'gray'
                          : Color.gray03,
                    }}>
                    {item.label}
                  </BoldText>
                </TouchableOpacity>
              );
            }}
          />
        </View>
        <View style={{backgroundColor: Color.gray05, height: 10}} />
        <View style={{marginVertical: 10, padding: 20}}>
          <BoldText
            style={{fontSize: 25, color: Color.gray04, marginBottom: 10}}>
            {activeVariety.label}
          </BoldText>
          {Object.keys(ITEMS_LIST).map((productItem, i) => {
            return (
              <View key={`product-${i}`} style={{marginVertical: 25}}>
                <VarietyItem productItem={ITEMS_LIST[productItem]} />
              </View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
}

export default VendorItems;
