import React, { useEffect } from 'react';
import { View } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import 'react-native-gesture-handler';
import {BoldText, LightText} from '../Components/Common/StyledTexts';

function Splash(props){

  useEffect(()=>{
    setTimeout(()=>{
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Home' })],
      });
      props.navigation.dispatch(resetAction);
    } , 3000);
  } , [0]);


  return(
    <View style={{ flex: 1  , borderWidth: 1}}>
      <View style={{ alignItems: 'center', justifyContent: 'center' , backgroundColor: 'white'}}>
        <View style={{borderWidth: 1 , width: "100%" , height: "100%" , justifyContent: "center" , alignItems: 'center'}}>
          <BoldText>Welcome To toneTag</BoldText>
        </View>
      </View>
    </View>
  )
}

export default Splash;

